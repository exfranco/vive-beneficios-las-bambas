import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-seguridad',
  templateUrl: './seguridad.component.html',
  styleUrls: ['./seguridad.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SeguridadComponent implements OnInit {

  public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Seguridad de Salud Pacifico EPS', '#', true, 'se1', 'seguridad'),
	      new listaBeneficio('Seguridad Oncológico Pacifico', '#', false, 'se2', 'seguridad'),
	      new listaBeneficio('Seguridad de Vida', '#', false, 'se3', 'seguridad'),
	      new listaBeneficio('Pies Saludables', '#', true, 'se4', 'seguridad'),
	      new listaBeneficio('Shakti Chana', '#', true, 'se5', 'seguridad'),
	      new listaBeneficio('Sabai Spa', '#', false, 'se6', 'seguridad'),
	      new listaBeneficio('Manamat Yoga', '#', true, 'se7', 'seguridad')
	    ];

  	}
  

  ngOnInit() {
  }

}
