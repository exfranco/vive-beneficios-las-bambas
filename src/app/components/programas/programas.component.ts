import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProgramasComponent implements OnInit {

	constructor(
		private _router: Router,
		private _route: ActivatedRoute
	){ 
  	
  	}

  	ngOnInit() {

	  	this._route.params.subscribe(params => {

	  		let id = params.id;
	  		console.log("el id del programa es "+id);

	  	});
	}
	
}
