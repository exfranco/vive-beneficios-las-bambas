import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-carrousel',
  templateUrl: 'Carrousel.component.html',
  styleUrls: ['Carrousel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarrouselComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
