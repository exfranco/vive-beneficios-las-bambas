import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaAzulComponent } from './CajaAzul.component';

describe('CajaAzulComponent', () => {
  let component: CajaAzulComponent;
  let fixture: ComponentFixture<CajaAzulComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaAzulComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaAzulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
