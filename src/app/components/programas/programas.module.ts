import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from '../../app.routing';
import { AppRoutingModule } from '../../app-routing.module';



import { ProgramasComponent } from './programas.component';
import { CajaAzulComponent } from './CajaAzul/CajaAzul.component';
import { RangoComponent } from './Rango/Rango.component';
import { CarrouselComponent } from './Carrousel/Carrousel.component'; 
import { CabeceraComponent } from './Cabecera/Cabecera.component'; 



@NgModule({
  declarations: [
    ProgramasComponent,
    CajaAzulComponent,
    RangoComponent,
    CarrouselComponent,
    CabeceraComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
  ],
  exports: [
    ProgramasComponent,
    CajaAzulComponent,
    RangoComponent,
    CarrouselComponent,
    CabeceraComponent
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [

  ]
})
export class ProgramasModule { 



}
