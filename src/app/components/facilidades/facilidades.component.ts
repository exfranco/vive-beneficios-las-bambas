import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-facilidades',
  templateUrl: './facilidades.component.html',
  styleUrls: ['./facilidades.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FacilidadesComponent implements OnInit {

  	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Facilidades 1', '#', true, 'fa1', 'facilidades'),
	      new listaBeneficio('Facilidades 2', '#', true, 'fa2', 'facilidades'),
	      new listaBeneficio('Facilidades 3', '#', false, 'fa3', 'facilidades'),
	      new listaBeneficio('Facilidades 4', '#', false, 'fa4', 'facilidades'),
	      new listaBeneficio('Facilidades 5', '#', false, 'fa5', 'facilidades'),
	      new listaBeneficio('Facilidades 6', '#', true, 'fa6', 'facilidades'),
	      new listaBeneficio('Facilidades 7', '#', false, 'fa7', 'facilidades')
	    ];

  	}

  	ngOnInit() {
  	}

}
