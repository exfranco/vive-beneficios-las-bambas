import { Component, ViewEncapsulation, OnInit, DoCheck } from '@angular/core';
import { InicioItem } from '../../model/inicioItem';

@Component({
  selector: 'app-itemlist',
  templateUrl: 'ItemList.component.html',
  styleUrls: ['ItemList.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemListComponent implements OnInit, DoCheck {

  public itemListas: Array<InicioItem>;

  constructor() {

    var url = "/vive-beneficios-las-bambas";

    this.itemListas = [
      new InicioItem('beneficios-en-salud', url+'/assets/images/home-item-1.png', 'Beneficios en Salud'),
      new InicioItem('beneficios-en-seguridad', url+'/assets/images/home-item-2.png', 'Beneficios en Seguridad'),
      new InicioItem('beneficios-en-diversion', url+'/assets/images/home-item-3.png', 'Beneficios en Diversion'),
      new InicioItem('beneficios-en-bienestar', url+'/assets/images/home-item-4.png', 'Beneficios en Bienestar'),
      new InicioItem('beneficios-en-deporte', url+'/assets/images/home-item-5.png', 'Beneficios en Deporte'),
      new InicioItem('beneficios-en-soluciones', url+'/assets/images/home-item-6.png', 'Beneficios en Soluciones'),
      new InicioItem('beneficios-en-facilidades', url+'/assets/images/home-item-7.png', 'Beneficios en Facilidades'),
      new InicioItem('beneficios-en-educacion', url+'/assets/images/home-item-8.png', 'Beneficios en Educacion')
    ];

  }

  ngOnInit() {
  	


  }

  ngDoCheck(){


  }

  

}
