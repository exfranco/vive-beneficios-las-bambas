import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from '../../app.routing';
import { AppRoutingModule } from '../../app-routing.module';
import { CarouselModule } from 'ngx-bootstrap/carousel';



import { InicioComponent } from './inicio.component';
import { BannerComponent } from './Banner/Banner.component';
import { ItemListComponent } from './ItemList/ItemList.component';
import { CarrouselComponent } from './Carrousel/Carrousel.component'; 



@NgModule({
  declarations: [
    InicioComponent,
    BannerComponent,
    ItemListComponent,
    CarrouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
    CarouselModule.forRoot()
  ],
  exports: [
    InicioComponent,
    BannerComponent,
    ItemListComponent,
    CarrouselComponent
  ],
  providers: [
    appRoutingProviders
   
  ],
  bootstrap: [

  ]
})
export class InicioModule { 



}
