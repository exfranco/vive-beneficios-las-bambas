import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: 'Banner.component.html',
  styleUrls: ['Banner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BannerComponent implements OnInit {

	title = 'Site de Beneficios de Las Bambas';

  constructor() { }

  ngOnInit() {
  }

}
