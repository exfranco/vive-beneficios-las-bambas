import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-diversion',
  templateUrl: './diversion.component.html',
  styleUrls: ['./diversion.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiversionComponent implements OnInit {

  	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Diversión 1', '#', true, 'di1', 'diversion'),
	      new listaBeneficio('Diversión 2', '#', true, 'di2', 'diversion'),
	      new listaBeneficio('Diversión 3', '#', false, 'di3', 'diversion'),
	      new listaBeneficio('Diversión 4', '#', false, 'di4', 'diversion'),
	      new listaBeneficio('Diversión 5', '#', false, 'di5', 'diversion'),
	      new listaBeneficio('Diversión 6', '#', true, 'di6', 'diversion'),
	      new listaBeneficio('Diversión 7', '#', false, 'di7', 'diversion')
	    ];

  	}

  ngOnInit() {
  }

}
