import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-salud',
  templateUrl: './salud.component.html',
  styleUrls: ['./salud.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SaludComponent implements OnInit {

	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Seguro de Salud Pacifico EPS', '#', false, 'sa1', 'salud'),
	      new listaBeneficio('Seguro Oncológico Pacifico', '#', true, 'sa2', 'salud'),
	      new listaBeneficio('Seguro de Vida', '#', true, 'sa3', 'salud'),
	      new listaBeneficio('Pies Saludables', '#', false, 'sa4', 'salud'),
	      new listaBeneficio('Shakti Chana', '#', false, 'sa5', 'salud'),
	      new listaBeneficio('Sabai Spa', '#', true, 'sa6', 'salud'),
	      new listaBeneficio('Manamat Yoga', '#', false, 'sa7', 'salud')
	    ];

  	}

  	ngOnInit() {
  	}

}
