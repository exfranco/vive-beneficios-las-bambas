import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-deporte',
  templateUrl: './deporte.component.html',
  styleUrls: ['./deporte.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DeporteComponent implements OnInit {

  	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Deporte 1', '#', true, 'de1', 'deporte'),
	      new listaBeneficio('Deporte 2', '#', true, 'de2', 'deporte'),
	      new listaBeneficio('Deporte 3', '#', false, 'de3', 'deporte'),
	      new listaBeneficio('Deporte 4', '#', false, 'de4', 'deporte'),
	      new listaBeneficio('Deporte 5', '#', false, 'de5', 'deporte'),
	      new listaBeneficio('Deporte 6', '#', true, 'de6', 'deporte'),
	      new listaBeneficio('Deporte 7', '#', false, 'de7', 'deporte')
	    ];

  	}

  	ngOnInit() {
  	}

}
