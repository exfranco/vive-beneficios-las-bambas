import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-bienestar',
  templateUrl: './bienestar.component.html',
  styleUrls: ['./bienestar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BienestarComponent implements OnInit {

  	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Bienestar 1', '#', false, 'bi1', 'bienestar'),
	      new listaBeneficio('Bienestar 2', '#', false, 'bi2', 'bienestar'),
	      new listaBeneficio('Bienestar 3', '#', true, 'bi3', 'bienestar'),
	      new listaBeneficio('Bienestar 4', '#', false, 'bi4', 'bienestar'),
	      new listaBeneficio('Bienestar 5', '#', false, 'bi5', 'bienestar'),
	      new listaBeneficio('Bienestar 6', '#', true, 'bi6', 'bienestar'),
	      new listaBeneficio('Bienestar 7', '#', false, 'bi7', 'bienestar')
	    ];

  	}

  	ngOnInit() {
  	}

}
