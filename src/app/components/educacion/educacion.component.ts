import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-educacion',
  templateUrl: './educacion.component.html',
  styleUrls: ['./educacion.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EducacionComponent implements OnInit {

  	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Educación 1', '#', true, 'ed1', 'educacion'),
	      new listaBeneficio('Educación 2', '#', true, 'ed2', 'educacion'),
	      new listaBeneficio('Educación 3', '#', false, 'ed3', 'educacion'),
	      new listaBeneficio('Educación 4', '#', false, 'ed4', 'educacion'),
	      new listaBeneficio('Educación 5', '#', false, 'ed5', 'educacion'),
	      new listaBeneficio('Educación 6', '#', true, 'ed6', 'educacion'),
	      new listaBeneficio('Educación 7', '#', false, 'ed7', 'educacion')
	    ];

  	}

  	ngOnInit() {
  	}

}
