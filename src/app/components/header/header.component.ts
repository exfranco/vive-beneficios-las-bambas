import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  menuPrincipalOpen(){
    document.getElementById("navbarText").classList.add("activado");
    document.body.classList.add("nonescroll");
  }

  menuPrincipalClose(){
  	document.getElementById("navbarText").classList.remove("activado");
    document.body.classList.remove("nonescroll");
  }

  buscadorOpen(){
    document.getElementById("buscador-caja").classList.add("activado");
    document.body.classList.add("nonescroll");
  }

  buscadorClose(){
    document.getElementById("buscador-caja").classList.remove("activado");
    document.body.classList.remove("nonescroll");
  }

}
