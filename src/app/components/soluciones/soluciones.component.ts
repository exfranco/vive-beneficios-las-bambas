import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { listaBeneficio } from '../model/listaBeneficios';

@Component({
  selector: 'app-soluciones',
  templateUrl: './soluciones.component.html',
  styleUrls: ['./soluciones.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SolucionesComponent implements OnInit {

  	public itemBeneficios: Array<listaBeneficio>;

  	constructor() { 

  		this.itemBeneficios = [
	      new listaBeneficio('Soluciones 1', '#', true, 'so1', 'soluciones'),
	      new listaBeneficio('Soluciones 2', '#', false, 'so2', 'soluciones'),
	      new listaBeneficio('Soluciones 3', '#', true, 'so3', 'soluciones'),
	      new listaBeneficio('Soluciones 4', '#', true, 'so4', 'soluciones'),
	      new listaBeneficio('Soluciones 5', '#', true, 'so5', 'soluciones'),
	      new listaBeneficio('Soluciones 6', '#', false, 'so6', 'soluciones'),
	      new listaBeneficio('Soluciones 7', '#', true, 'so7', 'soluciones')
	    ];

  	}

  	ngOnInit() {
  	}

}
