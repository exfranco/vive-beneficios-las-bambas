import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent{
  title = 'Vive Beneficios Las Bambas';

  public showHead: boolean = true;

  constructor(private router: Router) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'].indexOf('/programa') >= 0) {
          this.showHead = false;
        } else {
          this.showHead = true;
        }
      }
    });
  }



 

}
