import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './components/inicio/inicio.component';
import { SaludComponent } from './components/salud/salud.component';
import { SeguridadComponent } from './components/seguridad/seguridad.component';
import { DiversionComponent } from './components/diversion/diversion.component';
import { BienestarComponent } from './components/bienestar/bienestar.component';
import { DeporteComponent } from './components/deporte/deporte.component';
import { SolucionesComponent } from './components/soluciones/soluciones.component';
import { FacilidadesComponent } from './components/facilidades/facilidades.component';
import { EducacionComponent } from './components/educacion/educacion.component';
import { ErrorComponent } from './components/error/error.component';
import { ProgramasComponent } from './components/programas/programas.component';


const appRoutes: Routes = [
	{path: '', component: InicioComponent},
	{path: 'beneficios-en-salud', component: SaludComponent},
	{path: 'beneficios-en-seguridad', component: SeguridadComponent},
	{path: 'beneficios-en-diversion', component: DiversionComponent},
	{path: 'beneficios-en-bienestar', component: BienestarComponent},
	{path: 'beneficios-en-deporte', component: DeporteComponent},
	{path: 'beneficios-en-soluciones', component: SolucionesComponent},
	{path: 'beneficios-en-facilidades', component: FacilidadesComponent},
	{path: 'beneficios-en-educacion', component: EducacionComponent},
	{path: 'programa/:categoria/:id', component: ProgramasComponent},
	{path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);