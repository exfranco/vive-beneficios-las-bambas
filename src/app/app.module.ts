import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludComponent } from './components/salud/salud.component';
import { SeguridadComponent } from './components/seguridad/seguridad.component';
import { DiversionComponent } from './components/diversion/diversion.component';
import { BienestarComponent } from './components/bienestar/bienestar.component';
import { DeporteComponent } from './components/deporte/deporte.component';
import { SolucionesComponent } from './components/soluciones/soluciones.component';
import { FacilidadesComponent } from './components/facilidades/facilidades.component';
import { EducacionComponent } from './components/educacion/educacion.component';
import { ErrorComponent } from './components/error/error.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

import { InicioModule } from './components/inicio/inicio.module';
import { ProgramasModule} from './components/programas/programas.module';
import { CarouselModule } from 'ngx-bootstrap/carousel';


@NgModule({
  declarations: [
    AppComponent,
    SaludComponent,
    SeguridadComponent,
    DiversionComponent,
    BienestarComponent,
    DeporteComponent,
    SolucionesComponent,
    FacilidadesComponent,
    EducacionComponent,
    ErrorComponent,
    FooterComponent,
    HeaderComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
    InicioModule,
    ProgramasModule,
    CarouselModule.forRoot()
  ],
  providers: [
    appRoutingProviders,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 



}
